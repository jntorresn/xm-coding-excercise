XM CODING EXERCISE

On this Spring Boot Project, there is the implementation to handle all new trades executed by external broker.

[[_TOC_]]

# 1. Architecture

On picture below, there is a diagram of the project:

![alt text](docs/architecture.PNG)

# 2. Properties

For the correct working of this app, it is necessary to set up below parameters on _application.properties_ file.

Below parameters needs to be defined:

-  General

To configure the port where the app be loading:

```
server.port=(puerto)
```

- JPA

For JPA section (Java Persistance API) it is needed to adjust parameters as below, so Hibernate can interact correctly with H2DB.

```
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.jpa.database=H2
spring.jpa.hibernate.ddl-auto=validate
spring.jpa.show-sql=true
#spring.jpa.properties.hibernate.default_schema=XM
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.H2Dialect
```

-  H2 DB

As it is shown on initial diagram, this project uses as database, a H2 schema, so it is necessary to configure access for this db on below parameters:

```
spring.h2.console.enabled=true
spring.sql.init.platform=H2
spring.datasource.url=jdbc:h2:file:/data/XMDB
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=admin
spring.datasource.password=password
```
a) _console:_ to enable access to H2 UI on http://{server}:{port}/h2-console

b) _url:_ specifies db name (XMDB)

c) _user:_ usename to access data base

d) _password:_ user password to read/write on the data base.

 - Flyway

Flyway dependency is used to initalize data base, so it is in charge of executing scripts to create the trade table and its columns. Parameters to set here are similar to DB section:

```
spring.flyway.enabled=true
spring.flyway.url=jdbc:h2:file:/data/XMDB
spring.flyway.user=admin
spring.flyway.password=password
```
# 3. Database

On this project, a H2 Embedded DB is implemented, with an unique table, which can seen below:

![alt text](docs/Table.PNG)

# 4. Services

To test all the services, there is provided a POSTMAN collection on:

* [Test requests](src/main/resources/templates/XM.postman_collection.json)


# 5. References

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.0.2/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.0.2/maven-plugin/reference/html/#build-image)
* [Flyway Migration](https://docs.spring.io/spring-boot/docs/3.0.2/reference/htmlsingle/#howto.data-initialization.migration-tool.flyway)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/3.0.2/reference/htmlsingle/#data.sql.jpa-and-spring-data)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.0.2/reference/htmlsingle/#web)