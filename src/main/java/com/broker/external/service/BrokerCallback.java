package com.broker.external.service;

import java.util.UUID;

import com.broker.external.BrokerResponseCallback;
import com.broker.external.response.ExternalBrokerResponse;

public class BrokerCallback implements BrokerResponseCallback {

	private ExternalBrokerResponse response;
	private boolean networkIssue;

	public BrokerCallback() {
		this.response = new ExternalBrokerResponse();
		this.networkIssue = true;
	}

	@Override
	public void successful(UUID tradeId) {

		System.out.println("Trade " + tradeId + ": Succesful");

		this.networkIssue = false;
		this.response.setStatus("EXECUTED");
		this.response.setReason(null);
	}

	@Override
	public void unsuccessful(UUID tradeId, String reason) {

		System.out.println("Trade " + tradeId + ": Unsuccesful - " + reason);

		this.networkIssue = false;
		this.response.setStatus("NOT_EXECUTED");
		this.response.setReason(reason);
	}

	public ExternalBrokerResponse getResponse() {
		return response;
	}

	public void setResponse(ExternalBrokerResponse response) {
		this.response = response;
	}

	public boolean getNetworkIssue() {
		return networkIssue;
	}

	public void setNetworkIssue(boolean networkIssue) {
		this.networkIssue = networkIssue;
	}

}
