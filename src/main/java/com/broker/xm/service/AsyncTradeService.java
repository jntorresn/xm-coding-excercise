package com.broker.xm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.broker.external.response.ExternalBrokerResponse;
import com.broker.xm.model.Trade;
import com.broker.xm.model.TradeStatus;
import com.broker.xm.repository.TradeCrud;
import com.broker.xm.service.forward.ExecuteExternalBroker;

@Service
public class AsyncTradeService {

	@Autowired
	private TradeCrud tradeCrud;

	@Autowired
	private ExecuteExternalBroker executeExternalBroker;

	@Async
	public void executeTrade(Trade trade, String externalUri) {

		System.out.println("Start new Thread: " + Thread.currentThread().getName());

		tradeCrud.save(trade);
		ExternalBrokerResponse externalBrokerResponse = executeExternalBroker.requestExternalBroker(trade, externalUri);

		if (externalBrokerResponse.getStatus().equals("EXECUTED")) {

			tradeCrud.updateSuccesfulTrade(TradeStatus.EXECUTED, trade.getId());
		} else {

			tradeCrud.updateUnSuccesfulTrade(TradeStatus.NOT_EXECUTED, trade.getId(),
					externalBrokerResponse.getReason());
		}

	}

}
