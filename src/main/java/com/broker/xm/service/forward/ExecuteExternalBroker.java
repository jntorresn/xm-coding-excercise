package com.broker.xm.service.forward;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.broker.external.response.ExternalBrokerResponse;
import com.broker.xm.model.Trade;

@Service
public class ExecuteExternalBroker {
	
	private RestTemplate restTemplate = new RestTemplate();
	
	public ExternalBrokerResponse requestExternalBroker(Trade trade, String externalUri) {

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id", trade.getId());
		parameters.put("symbol", trade.getSymbol());
		parameters.put("quantity", trade.getQuantity());
		parameters.put("side", trade.getSide().toString());
		parameters.put("price", trade.getPrice());
		
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(parameters, headers);

		return this.restTemplate.postForObject(externalUri, entity, ExternalBrokerResponse.class);
	}

}
