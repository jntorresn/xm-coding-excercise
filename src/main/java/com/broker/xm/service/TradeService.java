package com.broker.xm.service;

import java.math.BigDecimal;
import java.net.URI;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.broker.xm.model.Trade;
import com.broker.xm.model.TradeSide;
import com.broker.xm.model.TradeStatus;
import com.broker.xm.network.request.RequestTrade;
import com.broker.xm.network.response.NewTradeResponse;
import com.broker.xm.network.response.TradeResponse;
import com.broker.xm.repository.TradeCrud;

import jakarta.persistence.EntityNotFoundException;

@Service
public class TradeService {

	private final String symbolIncorrect = "Symbol valid values: USD/JPY, EUR/USD";
	private final String quantityIncorrect = "Quantity must be greater than 0 and less than or equal to 1M";
	private final String priceIncorrect = "Price must be greater than 0";

	@Autowired
	private AsyncTradeService asyncTradeService;

	@Autowired
	private TradeCrud tradeCrud;

	public NewTradeResponse validateTrade(RequestTrade trade, TradeSide side) {

		String failedReason;

		if ((!trade.getSymbol().equals("EUR/USD")) && (!trade.getSymbol().equals("USD/JPY"))) {

			failedReason = symbolIncorrect;

		} else {

			if ((trade.getQuantity() <= 0) || (trade.getQuantity() > 1000000)) {

				failedReason = quantityIncorrect;

			} else {

				if (trade.getPrice() <= 0) {

					failedReason = priceIncorrect;
				} else {

					Trade newTrade = new Trade(UUID.randomUUID().toString(), trade.getQuantity(), trade.getSymbol(),
							side, BigDecimal.valueOf(trade.getPrice()), TradeStatus.PENDING_EXECUTION,
							new Timestamp(System.currentTimeMillis()), null);

					URI externalUri = ServletUriComponentsBuilder.fromCurrentServletMapping()
							.path("/api/external/newtrade").build().toUri();

					asyncTradeService.executeTrade(newTrade, externalUri.toString());
					return new NewTradeResponse(newTrade.getId());

				}
			}
		}

		return new NewTradeResponse(new ResponseEntity<String>(failedReason, HttpStatus.BAD_REQUEST));

	}

	public HashMap<String, String> getTradeStatus(String id) {

		HashMap<String, String> response = new HashMap<String, String>();

		try {
			response.put("status", tradeCrud.getReferenceById(id).getStatus().toString());

		} catch (EntityNotFoundException ex) {
			System.out.println("Trade with ID (" + id + ") not found");
			response.put("status", "NOT FOUND");

		}

		return response;
	}

	public TradeResponse getTrade(String id) {

		try {

			Trade trade = tradeCrud.getReferenceById(id);

			return new TradeResponse(trade.getId(), trade.getQuantity(), trade.getSymbol(), trade.getSide().toString(),
					trade.getPrice().doubleValue(), trade.getStatus().toString(), trade.getReason(),
					new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(trade.getTimeStamp()));

		} catch (EntityNotFoundException ex) {
			System.out.println("Trade with ID (" + id + ") not found");
			return null;

		}

	}
	
	public List<Trade> getTrades() {
		
		return tradeCrud.findAll();
	}

}
