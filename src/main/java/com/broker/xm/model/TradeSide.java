package com.broker.xm.model;

public enum TradeSide {
	BUY, SELL;
}
