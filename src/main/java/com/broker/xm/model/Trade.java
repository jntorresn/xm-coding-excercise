package com.broker.xm.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

@Table(name = "trade")
@Entity
public class Trade {

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "symbol")
	private String symbol;

	@Column(name = "side")
	@Enumerated(EnumType.STRING)
	private TradeSide side;

	@Column(name = "price")
	private BigDecimal price;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private TradeStatus status;
	
	@Column(name = "reason")
	private String reason;

	@Column(name = "timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Timestamp timeStamp;

	public Trade() {

	}

	public Trade(String id, int quantity, String symbol, TradeSide side, BigDecimal price, TradeStatus status,
			Timestamp timeStamp, String reason) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.symbol = symbol;
		this.side = side;
		this.price = price;
		this.status = status;
		this.timeStamp = timeStamp;
		this.reason = reason;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public TradeSide getSide() {
		return side;
	}

	public void setSide(TradeSide side) {
		this.side = side;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public TradeStatus getStatus() {
		return status;
	}

	public void setStatus(TradeStatus status) {
		this.status = status;
	}

	public Timestamp getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public String getReason() {
		return this.reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}

}
