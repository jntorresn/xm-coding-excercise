package com.broker.xm.model;

public enum TradeStatus {

	PENDING_EXECUTION, EXECUTED, NOT_EXECUTED

}
