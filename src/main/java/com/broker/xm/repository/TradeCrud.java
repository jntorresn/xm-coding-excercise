package com.broker.xm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.broker.xm.model.Trade;
import com.broker.xm.model.TradeStatus;

public interface TradeCrud extends JpaRepository<Trade, String> {

	@Transactional
	@Modifying
	@Query(value = "UPDATE Trade tr SET tr.status = :status WHERE tr.id = :uuid", nativeQuery = false)
	void updateSuccesfulTrade(TradeStatus status, String uuid);

	@Transactional
	@Modifying
	@Query(value = "UPDATE Trade tr SET tr.status = :status, tr.reason = :reason WHERE tr.id = :uuid", nativeQuery = false)
	void updateUnSuccesfulTrade(TradeStatus status, String uuid, String reason);

}
