package com.broker.xm.network.response;

public class TradeResponse {

	private String id;
	private int quantity;
	private String symbol;
	private String side;
	private double price;
	private String status;
	private String reason;
	private String timestamp;

	public TradeResponse() {

	}

	public TradeResponse(String id, int quantity, String symbol, String side, double price, String status,
			String reason, String timestamp) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.symbol = symbol;
		this.side = side;
		this.price = price;
		this.status = status;
		this.reason = reason;
		this.timestamp = timestamp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}
