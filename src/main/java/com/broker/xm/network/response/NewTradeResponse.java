package com.broker.xm.network.response;

import java.net.URI;
import java.util.HashMap;

import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public class NewTradeResponse {

	private ResponseEntity<?> httpResponse;

	public NewTradeResponse(ResponseEntity<?> httpResponse) {
		this.httpResponse = httpResponse;
	}

	public NewTradeResponse(String uuid) {

		URI location = ServletUriComponentsBuilder.fromCurrentServletMapping().path("/api/trades/{id}/status")
				.buildAndExpand(uuid).toUri();

		HashMap<String, String> body = new HashMap<>();
		body.put("location", location.toString());

		this.httpResponse = ResponseEntity.created(location).body(body);
	}

	public ResponseEntity<?> getHttpResponse() {
		return httpResponse;
	}

	public void setHttpResponse(ResponseEntity<?> httpResponse) {

		this.httpResponse = httpResponse;
	}

}
