package com.broker.xm.network.request;

public class RequestTrade {

	private String symbol;
	private int quantity;
	private double price;

	public RequestTrade(String symbol, int quantity, double price) {
		super();
		this.symbol = symbol;
		this.quantity = quantity;
		this.price = price;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
