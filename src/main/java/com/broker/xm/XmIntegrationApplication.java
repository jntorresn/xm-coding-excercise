package com.broker.xm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class XmIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmIntegrationApplication.class, args);
	}

}
