package com.broker.xm.rest;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.broker.xm.network.response.TradeResponse;
import com.broker.xm.service.TradeService;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST })
@RestController
@RequestMapping("/api/trades")
public class TradeRest {

	@Autowired
	private TradeService tradeService;

	@GetMapping("/{tradeId}/status")
	public ResponseEntity<?> getTradeStatus(@PathVariable("tradeId") String tradeId) {

		HashMap<String, String> status = tradeService.getTradeStatus(tradeId);

		if (status.get("status").equals("NOT FOUND")) {
			return new ResponseEntity<Object>(status, HttpStatus.FAILED_DEPENDENCY);

		} else {
			return new ResponseEntity<Object>(status, HttpStatus.OK);
		}
	}

	@GetMapping("/{tradeId}")
	public ResponseEntity<?> getTradeDetails(@PathVariable("tradeId") String tradeId) {

		TradeResponse trade = tradeService.getTrade(tradeId);

		if (trade == null) {
			return new ResponseEntity<String>("Trade not found", HttpStatus.FAILED_DEPENDENCY);

		} else {
			return new ResponseEntity<Object>(trade, HttpStatus.OK);
		}
	}

	@GetMapping("")
	public ResponseEntity<?> getTrades() {

		return new ResponseEntity<Object>(tradeService.getTrades(), HttpStatus.OK);
	}

}
