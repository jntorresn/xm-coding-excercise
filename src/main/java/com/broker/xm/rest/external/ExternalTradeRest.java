package com.broker.xm.rest.external;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.broker.external.BrokerTrade;
import com.broker.external.ExternalBroker;
import com.broker.external.response.ExternalBrokerResponse;
import com.broker.external.service.BrokerCallback;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST })
@RestController
@RequestMapping("/api/external")
public class ExternalTradeRest {

	@PostMapping(path = "/newtrade")
	public ResponseEntity<?> newtrade (@RequestBody BrokerTrade trade) {
		
		BrokerCallback brokerCallback = new BrokerCallback();
		ExternalBroker externalBroker = new ExternalBroker(brokerCallback);
		externalBroker.execute(trade);
		
		int waitTime = 0;
		
		try {
			while(brokerCallback.getNetworkIssue()) {
				Thread.sleep(1000);
				waitTime++;
				if(waitTime == 60) {
					System.out.println("Trade " + trade.getId() + ": Unsuccesful - trade expired");
					brokerCallback.getResponse().setStatus("NOT_EXECUTED");
					brokerCallback.getResponse().setReason("trade expired");
					brokerCallback.setNetworkIssue(false);
				}
			}
			
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
		return new ResponseEntity<ExternalBrokerResponse>(brokerCallback.getResponse(), HttpStatus.OK);
		
		
	}

}
