package com.broker.xm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.broker.xm.model.TradeSide;
import com.broker.xm.network.request.RequestTrade;
import com.broker.xm.service.TradeService;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST })
@RestController
@RequestMapping("/api")
public class NewTradeRest {

	@Autowired
	private TradeService tradeService;

	@PostMapping(path = "/sell")
	public ResponseEntity<?> sell(@RequestBody RequestTrade trade) {

		return tradeService.validateTrade(trade, TradeSide.SELL).getHttpResponse();

	}

	@PostMapping(path = "/buy")
	public ResponseEntity<?> buy(@RequestBody RequestTrade trade) {

		return tradeService.validateTrade(trade, TradeSide.BUY).getHttpResponse();
	}

}
