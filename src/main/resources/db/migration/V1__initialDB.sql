-- ---------------------------------
-- Project: XM Integration
-- DataBase: H2
-- Version: 1.0
-- Description: Data loaded by flyway
-- ---------------------------------

DROP TABLE trade IF EXISTS;

CREATE TABLE trade (

	id VARCHAR(255) PRIMARY KEY NOT NULL,
	quantity INT NOT NULL,
	symbol VARCHAR(255) NOT NULL,
	side VARCHAR(255) NOT NULL,
	price NUMERIC(38,5) NOT NULL,
	status VARCHAR(255),
	reason VARCHAR(255),
	timestamp TIMESTAMP NOT NULL
	
);
